-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 21, 2017 at 05:01 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_pariwisataapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_admin`
--

CREATE TABLE `tb_admin` (
  `id` int(11) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_admin`
--

INSERT INTO `tb_admin` (`id`, `username`, `password`) VALUES
(1, 'admin', 'c4ca4238a0b923820dcc509a6f75849b');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kabupaten`
--

CREATE TABLE `tb_kabupaten` (
  `id` int(11) NOT NULL,
  `nama_kabupaten` varchar(255) DEFAULT NULL,
  `foto` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kabupaten`
--

INSERT INTO `tb_kabupaten` (`id`, `nama_kabupaten`, `foto`) VALUES
(1, 'Banjarnegara', 'banjarnegara.jpg'),
(2, 'Banyumas', 'banyumas.jpg'),
(3, 'Batang', 'batang.jpg'),
(4, 'Blora', 'blora.jpg'),
(5, 'Boyolali', 'boyolali.jpg'),
(6, 'Brebes', 'brebes.jpg'),
(7, 'Cilacap', 'cilacap.jpg'),
(8, 'Demak', 'demak.jpg'),
(9, 'Grobogan', 'file_1509336130.jpg'),
(10, 'Jepara', 'jepara.jpg'),
(11, 'Karanganyar', 'karanganyar.jpg'),
(12, 'Kebumen', 'kebumen.jpg'),
(13, 'Kendal', 'kendal.jpg'),
(14, 'Klaten', 'klaten.jpg'),
(15, 'Kudus', 'kudus.jpg'),
(16, 'Magelang', 'magelang.jpg'),
(17, 'Pati', 'pati.jpg'),
(18, 'Pekalongan', 'pekalongan.jpg'),
(19, 'Pemalang', 'pemalang.jpg'),
(20, 'Purbalingga', 'purbalingga.jpg'),
(21, 'Purworejo', 'purworejo.jpg'),
(22, 'Rembang', 'rembang.jpg'),
(23, 'Semarang', 'semarang.jpg'),
(24, 'Sragen', 'sragen.jpg'),
(25, 'Sukoharjo', 'sukoharjo.jpg'),
(26, 'Tegal', 'tegal.jpg'),
(27, 'Temanggung', 'temanggung.jpg'),
(28, 'Wonogiri', 'wonogiri.jpg'),
(29, 'Wonosobo', 'wonosobo.jpg'),
(30, 'Kota Magelang', 'magelang.jpg'),
(31, 'Kota Pekalongan', 'pekalongan.jpg'),
(32, 'Kota Salatiga', 'file_1509336179.jpg'),
(33, 'Kota Semarang', 'semarang.jpg'),
(34, 'Kota Surakarta', 'file_1509336246.jpg'),
(35, 'Kota Tegal', 'tegal.jpg');

-- --------------------------------------------------------


CREATE TABLE `tb_level` (
  `id` int(11) NOT NULL,
  `nama_level` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_level`
--

INSERT INTO `tb_level` (`id`, `nama_level`) VALUES
(1, 'Super Admin'),
(2, 'Administrator'),
(3, 'User');

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `no_telp` varchar(12) DEFAULT NULL,
  `id_level` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id`, `nama`, `email`, `password`, `no_telp`, `id_level`) VALUES
(1, 'Markonahe', 'makrona@gmail.com', '123', '0897575757', 3),
(2, 'Ipung Purwana', 'ipungofficial@gmail.com', '123', '095855757575', 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_admin`
--
ALTER TABLE `tb_admin`
  ADD PRIMARY KEY (`id`);

--
--
-- Indexes for table `tb_kabupaten`
--
ALTER TABLE `tb_kabupaten`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_level`
--
ALTER TABLE `tb_level`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_level` (`id_level`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_admin`
--
ALTER TABLE `tb_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_kabupaten`
--
ALTER TABLE `tb_kabupaten`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `tb_level`
--
ALTER TABLE `tb_level`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--


--
-- Constraints for table `tb_user`
--
-- ALTER TABLE `tb_user`
--  ADD CONSTRAINT `tb_user_ibfk_1` FOREIGN KEY (`id_level`) REFERENCES `tb_level` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

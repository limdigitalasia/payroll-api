<?php

class Api_Setting_Company extends CI_Controller{

    //https://banyu.center/pariwisataapp/assets/images/wisata/
    
	//Load data dari database dengan fungsi construct
	function __construct(){
        parent::__construct();
        
        $this->load->model('setting/m_setting_company');
	}

    function companyIndex(){
        $this->load->view('api/setting/api-setting-company');
    }


    //Begin of Company 

    function getCompany()
    {
       
        $json_data = $this->m_setting_company->get_data()->result();
       
            $arr = array();
            foreach ($json_data as $result) {
            $arr[] = array(
                   'id' => $result->id,
                   'name' => $result->name,
                   'taxid' => $result->taxid,
                   'address' => $result->address
                    );
            }
               
        $data = json_encode($arr);
        echo "{\"company\":" . $data . "}";
    }

    public function insertCompany()
    {
        $row_count = 0;
        $insert_flag = FALSE; 
        $insert_data = "";
        $error_message = "";

        $unique_value = $this->input->post('taxid');

        $where_check = array('taxid' => $unique_value);
        $row_count = $this->m_setting_company->get_data($where_check)->num_rows();

        if ($row_count == 0){
            
            $data = array(
                'name' => $this->input->post('name'),
                'taxid' => $this->input->post('taxid'),
                'address' => $this->input->post('address'),
                'updated_date' => date ("Y-m-d H:i:s")
            );

            if (($insert_id = $this->m_setting_company->execute_insert($data)) == TRUE){
                
                $where =array('id'=>$insert_id);

                $json_data = $this->m_setting_company->get_data($where)->result();
        
                $arr = array();
                foreach ($json_data as $result) {
                $arr[] = array(
                    'id' => $result->id,
                    'name' => $result->name,
                    'taxid' => $result->taxid,
                    'address' => $result->address,
                    'updated_date' => date ("Y-m-d H:i:s")
                        );
                }
                
                $insert_data = json_encode($arr);

                $insert_flag = TRUE;
            }

        }
        else{
            $error_message = "Data already exist";
        }

        echo "[{\"insert_flag\":" . $insert_flag . "},{\"company_data\":" . $insert_data . "},{\"error_message\":" . $error_message . "}]";

    }

    public function updateCompany()
    {
        
        $update_flag = FALSE; 

        $id = $this->input->post('id'); 
       
        $data = array(
            'name' => $this->input->post('name'),
            'taxid' => $this->input->post('taxid'),
            'address' => $this->input->post('address'),
            'updated_date' => date ("Y-m-d H:i:s")
        );

        
        $where =array('id'=>$id); 

        if ($this->m_setting_company->execute_update($data,$where) == TRUE){
            $update_flag = TRUE;
        }

        
        $json_data = $this->m_setting_company->get_data($where)->result();
       
            $arr = array();
            foreach ($json_data as $result) {
            $arr[] = array(
                   'id' => $result->id,
                   'name' => $result->name,
                   'taxid' => $result->taxid,
                   'address' => $result->address
                    );
            }
               
        $update_data = json_encode($arr);


        echo "[{\"update_flag\":" . $update_flag . "},{\"company_data\":" . $update_data . "}]";

    }

    public function deleteCompany()
    {
        
        $delete_flag = FALSE; 

        $id = $this->input->post('id'); 
        
        $where =array('id'=>$id); 

        if ($this->m_setting_company->execute_delete($where) == TRUE){
            $delete_flag = TRUE;
        }

        echo "{\"delete_flag\":" . $delete_flag . "}";

    }

    //End of Company 

}

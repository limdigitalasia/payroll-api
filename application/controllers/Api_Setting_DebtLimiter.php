<?php

class Api_Setting_DebtLimiter extends CI_Controller{

    //https://banyu.center/pariwisataapp/assets/images/wisata/
    
	//Load data dari database dengan fungsi construct
	function __construct(){
        parent::__construct();
        
        $this->load->model('setting/m_setting_debtlimiter');
	}

    function debtlimiterIndex(){
        $this->load->view('api/setting/api-setting-debtlimiter');
    }

  
    //Begin of Debt Limiter 
    function getDebtLimiter()
    {
        $json_data = $this->m_setting_debtlimiter->get_data()->result();
       
        $arr = array();
        foreach ($json_data as $result) {
            $arr[] = array(
                'id' => $result->id,
                'min_workperiod' => $result->min_workperiod,
                'max_workperiod' => $result->max_workperiod,
                'debt_limit' => $result->debt_limit
            );
        }
               
        $data = json_encode($arr);
        echo "{\"debt_limiter\":" . $data . "}";
    }

    public function insertDebtLimiter()
    {
        $row_count = 0;
        $insert_flag = FALSE; 
        $insert_data = "";
        $error_message = "";

        $unique_value = $this->input->post('min_workperiod');

        $where_check = array('min_workperiod' => $unique_value);
        $row_count = $this->m_setting_debtlimiter->get_data($where_check)->num_rows();

        if ($row_count == 0){
            
            $data = array(
                'min_workperiod' => $this->input->post('min_workperiod'),
                'max_workperiod' => $this->input->post('max_workperiod'),
                'debt_limit' => $this->input->post('debt_limit'),
                'updated_date' => date ("Y-m-d H:i:s")
            );

            if (($insert_id = $this->m_setting_debtlimiter->execute_insert($data)) == TRUE){
                
                $where =array('id'=>$insert_id);

                $json_data = $this->m_setting_debtlimiter->get_data($where)->result();
        
                $arr = array();
                foreach ($json_data as $result) {
                    $arr[] = array(
                        'id' => $result->id,
                        'min_workperiod' => $result->min_workperiod,
                        'max_workperiod' => $result->max_workperiod,
                        'debt_limit' => $result->debt_limit,
                        'updated_date' => date ("Y-m-d H:i:s")
                    );
                }
                
                $insert_data = json_encode($arr);

                $insert_flag = TRUE;
            }

        }
        else{
            $error_message = "Data already exist";
        }

        echo "[{\"insert_flag\":" . $insert_flag . "},{\"debtlimiter_data\":" . $insert_data . "},{\"error_message\":" . $error_message . "}]";

    }

    public function updateDebtLimiter()
    {
        
        $update_flag = FALSE; 

        $id = $this->input->post('id'); 
        
        $data = array(
            'min_workperiod' => $this->input->post('min_workperiod'),
            'max_workperiod' => $this->input->post('max_workperiod'),
            'debt_limit' => $this->input->post('debt_limit'),
            'updated_date' => date ("Y-m-d H:i:s")
        );
        
        $where =array('id'=>$id); 

        if ($this->m_setting_debtlimiter->execute_update($data,$where) == TRUE){
            $update_flag = TRUE;
        }

        
        $json_data = $this->m_setting_debtlimiter->get_data($where)->result();
       
            $arr = array();
            foreach ($json_data as $result) {
                $arr[] = array(
                    'id' => $result->id,
                    'min_workperiod' => $result->min_workperiod,
                    'max_workperiod' => $result->max_workperiod,
                    'debt_limit' => $result->debt_limit,
                    'updated_date' => date ("Y-m-d H:i:s")
                );
            }
               
        $update_data = json_encode($arr);


        echo "[{\"update_flag\":" . $update_flag . "},{\"debtlimiter_data\":" . $update_data . "}]";

    }

    public function deleteDebtLimiter()
    {
        
        $delete_flag = FALSE; 

        $id = $this->input->post('id'); 
        
        $where =array('id'=>$id); 

        if ($this->m_setting_debtlimiter->execute_delete($where) == TRUE){
            $delete_flag = TRUE;
        }

        echo "{\"delete_flag\":" . $delete_flag . "}";

    }

     //End of Cash Account 
}

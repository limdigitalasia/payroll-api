<?php

class Api_Setting_DebtTotalLimiter extends CI_Controller{

    //https://banyu.center/pariwisataapp/assets/images/wisata/
    
	//Load data dari database dengan fungsi construct
	function __construct(){
        parent::__construct();
        
        $this->load->model('setting/m_setting_debttotallimiter');
	}

    function debttotallimiterIndex(){
        $this->load->view('api/setting/api-setting-debttotallimiter');
    }

  
    //Begin of Debt Limiter 
    function getDebtTotalLimiter()
    {
        $json_data = $this->m_setting_debttotallimiter->get_data()->result();
       
            $arr = array();
            foreach ($json_data as $result) {
                $arr[] = array(
                    'id' => $result->id,
                    'debt_limit' => $result->debt_limit,
                    'debt_interest' => $result->debt_interest,
                    'debt_min_installment' => $result->debt_min_installment
                );
            }
               
        $data = json_encode($arr);
        echo "{\"debt_total_limiter\":" . $data . "}";
    }

    public function insertDebtTotalLimiter()
    {
        $row_count = 0;
        $insert_flag = FALSE; 
        $insert_data = "";
        $error_message = "";

        $unique_value = $this->input->post('debt_limit');

        $where_check = array('debt_limit' => $unique_value);
        $row_count = $this->m_setting_debttotallimiter->get_data($where_check)->num_rows();

        if ($row_count == 0){
            
            $data = array(
                'debt_limit' => $this->input->post('debt_limit'),
                'debt_interest' => $this->input->post('debt_interest'),
                'debt_min_installment' => $this->input->post('debt_min_installment'),
                'updated_date' => date ("Y-m-d H:i:s")
            );

            if (($insert_id = $this->m_setting_debttotallimiter->execute_insert($data)) == TRUE){
                
                $where =array('id'=>$insert_id);

                $json_data = $this->m_setting_debttotallimiter->get_data($where)->result();
        
                $arr = array();
                foreach ($json_data as $result) {
                    $arr[] = array(
                        'id' => $result->id,
                        'debt_limit' => $result->debt_limit,
                        'debt_interest' => $result->debt_interest,
                        'debt_min_installment' => $result->debt_min_installment,
                        'updated_date' => date ("Y-m-d H:i:s")
                    );
                }
                
                $insert_data = json_encode($arr);

                $insert_flag = TRUE;
            }

        }
        else{
            $error_message = "Data already exist";
        }

        echo "[{\"insert_flag\":" . $insert_flag . "},{\"debttotallimiter_data\":" . $insert_data . "},{\"error_message\":" . $error_message . "}]";

    }

    public function updateDebtTotalLimiter()
    {
        
        $update_flag = FALSE; 

        $id = $this->input->post('id'); 
       
        $data = array(
            'debt_limit' => $this->input->post('debt_limit'),
            'debt_interest' => $this->input->post('debt_interest'),
            'debt_min_installment' => $this->input->post('debt_min_installment'),
            'updated_date' => date ("Y-m-d H:i:s")
        );
        
        $where =array('id'=>$id); 

        if ($this->m_setting_debttotallimiter->execute_update($data,$where) == TRUE){
            $update_flag = TRUE;
        }
        
        $json_data = $this->m_setting_debttotallimiter->get_data($where)->result();
       
            $arr = array();
            foreach ($json_data as $result) {
                $arr[] = array(
                    'id' => $result->id,
                    'debt_limit' => $result->debt_limit,
                    'debt_interest' => $result->debt_interest,
                    'debt_min_installment' => $result->debt_min_installment,
                    'updated_date' => date ("Y-m-d H:i:s")
                );
            }
               
        $update_data = json_encode($arr);


        echo "[{\"update_flag\":" . $update_flag . "},{\"debttotallimiter_data\":" . $update_data . "}]";

    }

    public function deleteDebtTotalLimiter()
    {
        
        $delete_flag = FALSE; 

        $id = $this->input->post('id'); 
        
        $where =array('id'=>$id); 

        if ($this->m_setting_debttotallimiter->execute_delete($where) == TRUE){
            $delete_flag = TRUE;
        }

        echo "{\"delete_flag\":" . $delete_flag . "}";

    }

     //End of Cash Account 
}

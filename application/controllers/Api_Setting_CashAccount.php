<?php

class Api_Setting_CashAccount extends CI_Controller{

    //https://banyu.center/pariwisataapp/assets/images/wisata/
    
	//Load data dari database dengan fungsi construct
	function __construct(){
        parent::__construct();
        
        $this->load->model('setting/m_setting_cashaccount');
	}

    function cashaccountIndex(){
        $this->load->view('api/setting/api-setting-cashaccount');
    }

  
    //Begin of Cash Account 
    function getCashAccount()
    {
        $json_data = $this->m_setting_cashaccount->get_data()->result();
       
            $arr = array();
            foreach ($json_data as $result) {
            $arr[] = array(
                   'id' => $result->id,
                   'account_number' => $result->account_number,
                   'account_name' => $result->account_name
                    );
            }
               
        $data = json_encode($arr);
        echo "{\"cash_account\":" . $data . "}";
    }

    public function insertCashAccount()
    {
        $row_count = 0;
        $insert_flag = FALSE; 
        $insert_data = "";
        $error_message = "";

        $unique_value = $this->input->post('account_number');

        $where_check = array('account_number' => $unique_value);
        $row_count = $this->m_setting_cashaccount->get_data($where_check)->num_rows();

        if ($row_count == 0){
            
            $data = array(
                'account_number' => $this->input->post('account_number'),
                'account_name' => $this->input->post('account_name'),
                'updated_date' => date ("Y-m-d H:i:s")
            );

            if (($insert_id = $this->m_setting_cashaccount->execute_insert($data)) == TRUE){
                
                $where =array('id'=>$insert_id);

                $json_data = $this->m_setting_cashaccount->get_data($where)->result();
        
                $arr = array();
                foreach ($json_data as $result) {
                $arr[] = array(
                    'id' => $result->id,
                    'account_number' => $result->account_number,
                    'account_name' => $result->account_name,
                    'updated_date' => date ("Y-m-d H:i:s")
                        );
                }
                
                $insert_data = json_encode($arr);

                $insert_flag = TRUE;
            }

        }
        else{
            $error_message = "Data already exist";
        }

        echo "[{\"insert_flag\":" . $insert_flag . "},{\"cash_account_data\":" . $insert_data . "},{\"error_message\":" . $error_message . "}]";

    }

    public function updateCashAccount()
    {
        
        $update_flag = FALSE; 

        $id = $this->input->post('id'); 
       
        $data = array(
            'account_number' => $this->input->post('account_number'),
            'account_name' => $this->input->post('account_name'),
            'updated_date' => date ("Y-m-d H:i:s")
        );

        
        $where =array('id'=>$id); 

        if ($this->m_setting_cashaccount->execute_update($data,$where) == TRUE){
            $update_flag = TRUE;
        }

        
        $json_data = $this->m_setting_cashaccount->get_data($where)->result();
       
            $arr = array();
            foreach ($json_data as $result) {
            $arr[] = array(
                   'id' => $result->id,
                   'account_number' => $result->account_number,
                   'account_name' => $result->account_name
                    );
            }
               
        $update_data = json_encode($arr);


        echo "[{\"update_flag\":" . $update_flag . "},{\"cash_account_data\":" . $update_data . "}]";

    }

    public function deleteCashAccount()
    {
        
        $delete_flag = FALSE; 

        $id = $this->input->post('id'); 
        
        $where =array('id'=>$id); 

        if ($this->m_setting_cashaccount->execute_delete($where) == TRUE){
            $delete_flag = TRUE;
        }

        echo "{\"delete_flag\":" . $delete_flag . "}";

    }

     //End of Cash Account 
}

<?php

class M_setting_debtlimiter extends CI_Model{

  var $tabel = 'tb_setting_debtlimiter';
  
  function get_data($where = null){
    $where_not_deleted =array('is_deleted'=>FALSE); 
    return $this->db->where($where_not_deleted)
                    ->order_by('id',"desc")
                  // ->limit(1)
                    ->get_where('tb_setting_debtlimiter', $where)
                    ;
  }
  
  function execute_update($data,$where){
    $this->db->where($where);
    $this->db->update($this->tabel, $data);
    return TRUE;
  }

  function execute_delete($where){
    $data = array('is_deleted'=>TRUE);
    $this->db->where($where);
    $this->db->update($this->tabel,$data);
    return TRUE;
  }

  function execute_insert($data){
    $this->db->insert($this->tabel,$data);
    $insert_id = $this->db->insert_id();
    return $insert_id;
  }

}

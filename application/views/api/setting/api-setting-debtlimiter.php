<html>
<body>
    <div>
        <p><b>API Setting - Debt Limiter</b></p>
        <br>
        <table>
            <tr>
                <td>GET Data </td>
                <td><a href="<?php echo base_url();?>api_setting_debtlimiter/getDebtLimiter" target="_blank" style="font-style:underline;">View</a></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>INSERT Data</td>
            </tr>
            <tr>
                <td>UPDATE Data</td>
            </tr>
            <tr>
                <td>DELETE Data</td>
            </tr>
        </table>

        <br> <br>
        
        <p><b>API Setting - Debt Total Limiter</b></p>
        <br>
        <table>
            <tr>
                <td>GET Data </td>
                <td><a href="<?php echo base_url();?>api_setting_debttotallimiter/getDebtTotalLimiter" target="_blank" style="font-style:underline;">View</a></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>INSERT Data</td>
            </tr>
            <tr>
                <td>UPDATE Data</td>
            </tr>
            <tr>
                <td>DELETE Data</td>
            </tr>
        </table>
    </div>
    <div>
        <br><br>
        <a href="<?php echo base_url();?>api_setting"><button style="cursor:pointer;">&#60;&#60; Back to API Setting</button></a>
        <br><br>
        <a href="<?php echo base_url();?>"><button style="cursor:pointer;">&#60;&#60; Back to API List</button></a>
    </div>
</body>
</html>

<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Dashboard
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Dashboard</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <!-- Small boxes (Stat box) -->
  
  <!-- /.row -->
  <!-- Main row -->
  <div class="row">
    <!-- Left col -->
    <form action="<?php echo base_url(). 'hotel/tambah_aksi'; ?> " method="post"  enctype="multipart/form-data">
    <section class="col-lg-6 connectedSortable">

      
  		<table class="table col-md-6">
            
          <tr>
  				<td>Kabupaten</td>
  				<td><select class="form-control" name="id_kabupaten" id="id_kabupaten" required="">
                    <option value="">Pilih</option>
                    <?php foreach($kabupaten as $row) { ?>
                    <option value="<?php echo $row->id; ?>" ><?php echo $row->nama_kabupaten;?></option>
                    <?php } ?>
                    </select>
          </td>
  		    </tr>
          
  		    <tr>
  				<td>Nama hotel</td>
  				<td><input type="text" class="form-control" name="nama_hotel" required="" value=""></td>
          </tr>
          
          <tr>
  				<td>Tipe hotel</td>
  				<td><input type="text" class="form-control" name="tipe_hotel" required="" value=""></td>
          </tr>

          <tr>
          <tr>
  				<td>Tarif hotel</td>
  				<td><input type="text" class="form-control" name="tarif" required="" value=""></td>
          </tr>

          
          <tr>
  				<td>Alamat</td>
  				<td><textarea type="text" class="form-control" name="alamat" required=""></textarea></td>
          </tr>

          <tr>
  				<td>NO.Telp</td>
  				<td><input type="text" class="form-control" name="no_telp" required="" value=""></td>
          </tr>
          </table>
          </section>
          <section class="col-lg-6 connectedSortable">
          <table class="table col-md-6">
          <tr>
  				<td>Whatsapp</td>
  				<td><input type="text" class="form-control" name="no_wa" required="" value=""></td>
          </tr>

          <tr>
  				<td>Facebook</td>
  				<td><input type="text" class="form-control" name="facebook" required="" value=""></td>
          </tr>

          <tr>
  				<td>Instagram</td>
  				<td><input type="text" class="form-control" name="instagram" required="" value=""></td>
          </tr>

          <tr>
  				<td>Twitter</td>
  				<td><input type="text" class="form-control" name="twitter" required="" value=""></td>
          </tr>

          <tr>
  				<td>Email</td>
  				<td><input type="text" class="form-control" name="email" required="" value=""></td>
          </tr>

          <tr>
  				<td>Website</td>
  				<td><input type="text" class="form-control" name="website" required="" value=""></td>
          </tr>
          
          <tr>
  				<td>Foto</td>
  				<td><input type="file" class="form-control" name="filefoto" required="" value=""></td>
  		    </tr>

          <tr>
  				<td>Keterangan</td>
  				<td><textarea type="text" class="form-control" name="keterangan" required=""></textarea></td>
          </tr>

  		    
  		
    </table>
    </section>
    <table class="table col-lg-12">
    <tr>
  				<td><input type="submit" class="btn btn-success" value="Tambah">
                <button class="btn btn-danger" value=""><a href="<?php echo base_url(). 'hotel'; ?>" style="color:white">Batal</a></button></td>
  		    </tr>
    </table>
  	</form>


  
    <!-- right col -->
  </div>
  <!-- /.row (main row) -->

</section>
<!-- /.content -->
</div>



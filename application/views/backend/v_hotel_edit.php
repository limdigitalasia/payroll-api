
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Dashboard
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Dashboard</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <!-- Small boxes (Stat box) -->
  
  <!-- /.row -->
  <!-- Main row -->
  <div class="row">
    <!-- Left col -->
    <section class="col-lg-12 connectedSortable">

    <?php
    foreach ($hotel as $s) { ?>

    <form action="<?php echo base_url(). 'hotel/edit_aksi'; ?> " method="post" id="form-satuan" enctype="multipart/form-data">
        <table class="table table-striped ">
      <input type="hidden" name="id" class="form-control" value="<?php echo $s->id;?>">
      
      <tr>
      <td>Kabupaten</td>
      <td><select class="form-control" name="id_kabupaten">
          <option value="">Pilih</option>
            <?php foreach($kabupaten as $row) {
                if($row->id == $s->id_kabupaten){ ?>
                <option value="<?php echo $row->id; ?>" selected=selected ><?php echo $row->id.'-'.$row->nama_kabupaten;?></option>
                <?php

                }else{
                  ?>
                <option value="<?php echo $row->id; ?>" ><?php echo $row->id.'-'.$row->nama_kabupaten;?></option>
            <?php }} ?>
        </select>
      </td>
      </tr>

      

      <tr>
      <td>Nama Hotel</td>
      <td><input type="text" class="form-control" name="nama_hotel" required="required" value="<?php echo $s->nama_hotel ?>"></td>
      </tr>
      <tr>
      <td>Tipe Hotel</td>
      <td><input type="text" class="form-control" name="tipe_hotel" required="required" value="<?php echo $s->tipe_hotel ?>"></td>
      </tr>
      <tr>
      <td>Tarif</td>
      <td><input type="text" class="form-control" name="tarif" required="required" value="<?php echo $s->tarif ?>"></td>
      </tr>
      
      <tr>
      <td>Alamat</td>
      <td><textarea type="text" class="form-control" name="alamat" required="required" value=""><?php echo $s->alamat ?></textarea>
      </tr>
      <tr>
      <td>No Telp</td>
      <td><input type="text" class="form-control" name="no_telp" required="required" value="<?php echo $s->no_telp ?>"></td>
      </tr>
      <tr>
      <td>Whatsapp</td>
      <td><input type="text" class="form-control" name="no_wa" required="required" value="<?php echo $s->no_wa ?>"></td>
      </tr>
      <tr>
      <td>Instagram</td>
      <td><input type="text" class="form-control" name="instagram" required="required" value="<?php echo $s->instagram ?>"></td>
      </tr>
      <tr>
      <td>Facebook</td>
      <td><input type="text" class="form-control" name="facebook" required="required" value="<?php echo $s->facebook ?>"></td>
      </tr>
      <tr>
      <td>Twitter</td>
      <td><input type="text" class="form-control" name="twitter" required="required" value="<?php echo $s->twitter ?>"></td>
      </tr>
      <tr>
      <td>Email</td>
      <td><input type="text" class="form-control" name="email" required="required" value="<?php echo $s->email ?>"></td>
      </tr>
      
      <tr>
      <td>Website</td>
      <td><input type="text" class="form-control" name="website" required="required" value="<?php echo $s->website ?>"></td>
      </tr>
      <tr>
      

      <tr>
      <td>Foto</td>
      <td><input type="file" class="form-control" name="filefoto" ></td>
      <input type="hidden" name="filelama" class="form-control" value="<?php echo $s->foto;?>">

      <tr>
      <td></td>
      <td><img src="<?php echo base_url(). 'assets/images/hotel/'?><?php echo $s->foto?>" alt="" width="300"></td>
      </tr>
      </tr>
      
      <tr>
      <td>Keterangan</td>
      <td><textarea type="text" class="form-control" name="keterangan" required="required" value=""><?php echo $s->keterangan ?></textarea>
      </tr>
      
            
            <tr>
                <td></td>
                <td><input type="submit" class="btn btn-success" value="Edit">
        <button class="btn btn-danger" value=""><a href="<?php echo base_url(). 'hotel'; ?>" style="color:white">Batal</a></button></td>

            </tr>
      <?php } ?>
        </table>

    </form>


   </section>
    <!-- right col -->
  </div>
  <!-- /.row (main row) -->

</section>
<!-- /.content -->
</div>



-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 22, 2019 at 04:29 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_payroll`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_admin`
--

CREATE TABLE `tb_admin` (
  `id` int(11) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_admin`
--

INSERT INTO `tb_admin` (`id`, `username`, `password`) VALUES
(1, 'admin', 'c4ca4238a0b923820dcc509a6f75849b');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kabupaten`
--

CREATE TABLE `tb_kabupaten` (
  `id` int(11) NOT NULL,
  `nama_kabupaten` varchar(255) DEFAULT NULL,
  `foto` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kabupaten`
--

INSERT INTO `tb_kabupaten` (`id`, `nama_kabupaten`, `foto`) VALUES
(1, 'Banjarnegara', 'banjarnegara.jpg'),
(2, 'Banyumas', 'banyumas.jpg'),
(3, 'Batang', 'batang.jpg'),
(4, 'Blora', 'blora.jpg'),
(5, 'Boyolali', 'boyolali.jpg'),
(6, 'Brebes', 'brebes.jpg'),
(7, 'Cilacap', 'cilacap.jpg'),
(8, 'Demak', 'demak.jpg'),
(9, 'Grobogan', 'file_1509336130.jpg'),
(10, 'Jepara', 'jepara.jpg'),
(11, 'Karanganyar', 'karanganyar.jpg'),
(12, 'Kebumen', 'kebumen.jpg'),
(13, 'Kendal', 'kendal.jpg'),
(14, 'Klaten', 'klaten.jpg'),
(15, 'Kudus', 'kudus.jpg'),
(16, 'Magelang', 'magelang.jpg'),
(17, 'Pati', 'pati.jpg'),
(18, 'Pekalongan', 'pekalongan.jpg'),
(19, 'Pemalang', 'pemalang.jpg'),
(20, 'Purbalingga', 'purbalingga.jpg'),
(21, 'Purworejo', 'purworejo.jpg'),
(22, 'Rembang', 'rembang.jpg'),
(23, 'Semarang', 'semarang.jpg'),
(24, 'Sragen', 'sragen.jpg'),
(25, 'Sukoharjo', 'sukoharjo.jpg'),
(26, 'Tegal', 'tegal.jpg'),
(27, 'Temanggung', 'temanggung.jpg'),
(28, 'Wonogiri', 'wonogiri.jpg'),
(29, 'Wonosobo', 'wonosobo.jpg'),
(30, 'Kota Magelang', 'magelang.jpg'),
(31, 'Kota Pekalongan', 'pekalongan.jpg'),
(32, 'Kota Salatiga', 'file_1509336179.jpg'),
(33, 'Kota Semarang', 'semarang.jpg'),
(34, 'Kota Surakarta', 'file_1509336246.jpg'),
(35, 'Kota Tegal', 'tegal.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tb_level`
--

CREATE TABLE `tb_level` (
  `id` int(11) NOT NULL,
  `nama_level` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_level`
--

INSERT INTO `tb_level` (`id`, `nama_level`) VALUES
(1, 'Super Admin'),
(2, 'Administrator'),
(3, 'User');

-- --------------------------------------------------------

--
-- Table structure for table `tb_setting_cashaccount`
--

CREATE TABLE `tb_setting_cashaccount` (
  `id` tinyint(4) NOT NULL,
  `account_number` varchar(20) DEFAULT NULL,
  `account_name` varchar(100) DEFAULT NULL,
  `updated_date` datetime NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_setting_cashaccount`
--

INSERT INTO `tb_setting_cashaccount` (`id`, `account_number`, `account_name`, `updated_date`, `is_deleted`) VALUES
(1, '10.1', 'Kas', '2019-05-20 00:00:00', 0),
(2, '10.4', ' Bank Danamon', '2019-05-22 17:31:58', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_setting_company`
--

CREATE TABLE `tb_setting_company` (
  `id` tinyint(4) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `taxid` varchar(30) DEFAULT NULL,
  `address` text,
  `updated_date` datetime NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_setting_company`
--

INSERT INTO `tb_setting_company` (`id`, `name`, `taxid`, `address`, `updated_date`, `is_deleted`) VALUES
(1, 'PT. Inverno Gadget Shop 2', '12333445533445', 'JL. Kapten Japa Gg Ciungwanara III / No . 1', '2019-05-16 00:00:00', 1),
(2, 'PT. Inverno Shop 1', '56411', 'JL Tomang Raya 2', '2019-05-22 17:35:08', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_setting_debtlimiter`
--

CREATE TABLE `tb_setting_debtlimiter` (
  `id` int(11) NOT NULL,
  `min_workperiod` smallint(6) DEFAULT NULL,
  `max_workperiod` smallint(6) DEFAULT NULL,
  `debt_limit` bigint(20) DEFAULT NULL,
  `updated_date` datetime NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_setting_debtlimiter`
--

INSERT INTO `tb_setting_debtlimiter` (`id`, `min_workperiod`, `max_workperiod`, `debt_limit`, `updated_date`, `is_deleted`) VALUES
(1, 0, 12, 1000000, '2019-05-21 05:00:00', 0),
(2, 6, 24, 3000000, '2019-05-22 21:04:37', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_setting_debttotallimiter`
--

CREATE TABLE `tb_setting_debttotallimiter` (
  `id` int(11) NOT NULL,
  `debt_limit` bigint(20) DEFAULT NULL,
  `debt_interest` decimal(10,0) DEFAULT NULL,
  `debt_min_installment` decimal(10,0) DEFAULT NULL,
  `updated_date` datetime NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_setting_debttotallimiter`
--

INSERT INTO `tb_setting_debttotallimiter` (`id`, `debt_limit`, `debt_interest`, `debt_min_installment`, `updated_date`, `is_deleted`) VALUES
(2, 600000, '11', '12', '2019-05-22 21:01:24', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_setting_workingday`
--

CREATE TABLE `tb_setting_workingday` (
  `id` int(11) NOT NULL,
  `workday_week` tinyint(4) DEFAULT NULL,
  `updated_date` datetime NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `no_telp` varchar(12) DEFAULT NULL,
  `id_level` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id`, `nama`, `email`, `password`, `no_telp`, `id_level`) VALUES
(1, 'Markonahe', 'makrona@gmail.com', '123', '0897575757', 3),
(2, 'Ipung Purwana', 'ipungofficial@gmail.com', '123', '095855757575', 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_admin`
--
ALTER TABLE `tb_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_kabupaten`
--
ALTER TABLE `tb_kabupaten`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_level`
--
ALTER TABLE `tb_level`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_setting_cashaccount`
--
ALTER TABLE `tb_setting_cashaccount`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_setting_company`
--
ALTER TABLE `tb_setting_company`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_setting_debtlimiter`
--
ALTER TABLE `tb_setting_debtlimiter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_setting_debttotallimiter`
--
ALTER TABLE `tb_setting_debttotallimiter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_setting_workingday`
--
ALTER TABLE `tb_setting_workingday`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_level` (`id_level`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_admin`
--
ALTER TABLE `tb_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_kabupaten`
--
ALTER TABLE `tb_kabupaten`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `tb_level`
--
ALTER TABLE `tb_level`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_setting_cashaccount`
--
ALTER TABLE `tb_setting_cashaccount`
  MODIFY `id` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tb_setting_company`
--
ALTER TABLE `tb_setting_company`
  MODIFY `id` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_setting_debtlimiter`
--
ALTER TABLE `tb_setting_debtlimiter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_setting_debttotallimiter`
--
ALTER TABLE `tb_setting_debttotallimiter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_setting_workingday`
--
ALTER TABLE `tb_setting_workingday`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
